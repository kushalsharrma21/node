var express = require("express");
var cookieParser = require('cookie-parser')
var app = express();
const { validationResult } = require('express-validator');
var mysql = require("mysql");
const e = require("express");
app.use(cookieParser());

var con = mysql.createConnection({
    host: process.env.HOST,
    user: process.env.USER,
    password: process.env.PASSWORD,
    database:process.env.DATABASE

  });

  con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
  });


exports.create =function(req,res){
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
     // return res.status(400).json({ errors: errors.array() });
     res.render('index', { err: errors.array()}); 
    }
    else
    {
       // return res.status(400).json({ success: "success" });
      var email=req.body.email;
      var pass=req.body.pass;
      var img=req.file.originalname;

       con.query(`SELECT * FROM student where email='${email}'` , function (err, result, fields) {
        if (result.length >0 ){
          res.render('index', { email_error: "Email Already exists"});
         }else
        {
            var sql = `INSERT INTO student (email, password, img, role) VALUES ('${email}', '${pass}', '${img}', "user")`;
            con.query(sql, function (err, result) {
            if (err) {
              throw err;
            }else
              {
            res.redirect('log_in'); 
              }
            });
        }
       });
    }
};

exports.login =function(req,res){
  res.render("log_in");
};

exports.logout =function(req,res){
  req.session.destroy();
  res.redirect("log_in");
};

exports.dash =function(req,res){
  if(req.session.role=="user")
  {
    var id=req.session.user_id;
    con.query(`SELECT * FROM student where id='${id}'` , function (err, result, fields) {
      if (result.length >0 ){
      res.render("dashboard", {user_data:result});
      }
  })
}
else if(req.session.role=="admin")
  {
    con.query(`SELECT * FROM student` , function (err, result, fields) {
      if (result.length >0 ){
      res.render("dashboard", {admin_data:result});
      }
     });
  }

  else
  {
    res.redirect("log_in");
  }
};

exports.log_in =function(req,res, next){
  var email=req.body.email;
  var pass=req.body.pass;
 
 con.query(`SELECT * FROM student where email='${email}' AND password='${pass}'` , function (err, result, fields) {
  if (result.length >0 ){
    for (const login_row of result){
    var id=login_row.id;
    var role=login_row.role;
    req.session.user_id=id;
    req.session.role=role;
   res.redirect('dashboard');
    }
    
   }else
  { 
    res.render('log_in', { invalid:"invalid login details" }); 
  }

 });
  
};



// exports.put = function(req,res){
//    req.session.count="kushla";
//    req.session.count1="sharma";
//   return res.status(400).json({ success:"okay"});
// };

exports.get =function(req,res){
  return res.status(400).json({ success: req.session });
};

exports.forget =function(req,res){
  delete  req.session.email; 
  return res.status(400).json({ success: req.session });
};

// exports.y =function(req,res){
//   return res.status(400).json({ success: res.flash('info', 'Flash is back!')});
// };

// exports.flashget =function(req,res){
//   return res.status(400).json({ success: res.flash('info')});
// };
